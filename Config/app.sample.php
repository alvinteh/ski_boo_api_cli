<?php
/**
 *   Add the following to app.php.
 **/

define('API_SERVER_URL', LOCAL_API_SERVER_URL);
define('API_CLIENT_KEY', LOCAL_API_CLIENT_KEY);
define('API_CLIENT_SECRET', LOCAL_API_CLIENT_SECRET);