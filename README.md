SKI - BOO - API CLI (Ski Japan - Booking System - API Client)
----------------


Description
----------------

* This repository is an API client wrapper for CakePHP.
* This repository typically should not be cloned on its own but as a CakePHP plugin through Composer.


Preparation Steps
----------------

* (N.A.)