<?php
App::uses('Component', 'Controller');

class ApiComponent extends Component {
    
    public function call($method, $args = null) {
        //Form request string
        $requestString = '';
        
        if (isset($args)) {
            ksort($args);
        
            foreach ($args as $param => $arg) {
                if (is_array($arg)) {
                    continue;
                }
        
                $requestString .= $param . '=' . $arg . '&';
            }
        
            $requestString = rtrim($requestString, '&');
        }
        
        //Form request
        $request = array();
        $request['key'] = API_CLIENT_KEY;
        $request['args'] = $args;
        $request['timestamp'] = time();
        $request['signature'] = hash('sha256', API_CLIENT_SECRET . '|' . $method . '|' . $requestString . '|' . $request['timestamp']);
        
        //Make request
        $ch = curl_init();
        
        curl_setopt($ch, CURLOPT_URL, API_SERVER_URL . $method);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($request));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        $output = curl_exec($ch);
        
        curl_close($ch);
        
        $data = json_decode($output);
        
        if ($data === null) {
            $data = $output; 
        }
        else {
            $data = $this::convertToArray((array) $data);
        }
        
        return $data;
    }
    
    
    private static function convertToArray($object) {
        if (is_object($object)) {
            $object = get_object_vars($object);
        }
    
        return is_array($object) ? array_map(__METHOD__, $object) : $object;
    }
    
}